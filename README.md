LibertyCode_Config
==================



Description
-----------

Library contains configuration components, 
allows to get value from specified key.

---



Requirement
-----------

- Script language: PHP: version 7 || 8

---



Installation
------------

Several ways are possible:

#### Composer

1. Requirement
    
    It requires composer installation.
    For more information: https://getcomposer.org
    
2. Command: Move in project root path
    
    ```sh
    cd "<project_root_path>"
    ```

3. Command: Installation
    
    ```sh
    php composer.phar require liberty_code/config ["<version>"]
    ```
    
4. Note

    - Include vendor
        
        If project uses composer, 
        vendor must be included:
        
        ```php
        require_once('<project_root_path>/vendor/autoload.php');
        ```
    
    - Configuration
    
        Installation command allows to add, 
        on composer file "<project_root_path>/composer.json",
        following configuration:
        
        ```json
        {
            "require": {
                "liberty_code/config": "<version>"
            }
        }
        ```

#### Include

1. Download
    
    - Download following repository.
    - Put it on repository root path.
    
2. Include source
    
    ```php
    require_once('<repository_root_path>/include/Include.php');
    ```

---



Usage
-----

#### Configuration

Configuration allows to retrieve values from keys.

_Elements_

- Config
    
    Configuration allows to design basic configuration class, 
    with features to retrieve value, from specified key. 
    
- DataConfig

    Extends configuration features.
    It uses data features to get values.
    
- RegisterConfig

    Extends configuration features.
    It uses register features to get values.

- CacheConfig

    Extends configuration features.
    It uses cache repository features to get values.
    
_Example_

```php
// Get data
use liberty_code\data\data\table\path\model\PathTableData;
$data = new PathTableData();
...
// Set data source
$data->setDataSrc(...);
...
// Get data configuration
use liberty_code\config\config\data\model\DataConfig;
$config = new DataConfig($data);
...
// Show specified value
echo($config->getValue('... string key'));
...
```

---


