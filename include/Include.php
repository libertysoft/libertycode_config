<?php

// Init var
$strRootPath = dirname(__FILE__) . '/..';

// Include class
include($strRootPath . '/src/config/library/ConstConfig.php');
include($strRootPath . '/src/config/exception/KeyInvalidFormatException.php');
include($strRootPath . '/src/config/api/ConfigInterface.php');
include($strRootPath . '/src/config/model/DefaultConfig.php');

include($strRootPath . '/src/config/data/library/ConstDataConfig.php');
include($strRootPath . '/src/config/data/exception/DataInvalidFormatException.php');
include($strRootPath . '/src/config/data/model/DataConfig.php');

include($strRootPath . '/src/config/register/library/ConstRegisterConfig.php');
include($strRootPath . '/src/config/register/exception/RegisterInvalidFormatException.php');
include($strRootPath . '/src/config/register/model/RegisterConfig.php');

include($strRootPath . '/src/config/cache/library/ConstCacheConfig.php');
include($strRootPath . '/src/config/cache/exception/RepositoryInvalidFormatException.php');
include($strRootPath . '/src/config/cache/model/CacheConfig.php');