<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\config\config\register\library;



class ConstRegisterConfig
{
    // Data constants
    const DATA_KEY_DEFAULT_REGISTER = 'objRegister';



    // Exception message constants
    const EXCEPT_MSG_REGISTER_INVALID_FORMAT = 'Following register "%1$s" invalid! It must be a register object, not null.';
}