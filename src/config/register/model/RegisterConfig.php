<?php
/**
 * Description :
 * This class allows to define register configuration class.
 * Register configuration uses specified register, to retrieve value.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\config\config\register\model;

use liberty_code\config\config\model\DefaultConfig;

use liberty_code\register\register\api\RegisterInterface;
use liberty_code\config\config\exception\KeyInvalidFormatException;
use liberty_code\config\config\register\library\ConstRegisterConfig;
use liberty_code\config\config\register\exception\RegisterInvalidFormatException;



/**
 * @method RegisterInterface getObjRegister() Get register object.
 * @method void setObjRegister(RegisterInterface $objRegister) Set register object.
 */
class RegisterConfig extends DefaultConfig
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Constructor / Destructor / Others
	// ******************************************************************************
	
	/**
	 * Constructor
	 *
     * @param RegisterInterface $objRegister
     */
	public function __construct(RegisterInterface $objRegister)
	{
		// Call parent constructor
		parent::__construct();

		// Hydrate register
		$this->setObjRegister($objRegister);
	}
	
	
	
	
	
    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstRegisterConfig::DATA_KEY_DEFAULT_REGISTER))
        {
            $this->__beanTabData[ConstRegisterConfig::DATA_KEY_DEFAULT_REGISTER] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstRegisterConfig::DATA_KEY_DEFAULT_REGISTER
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstRegisterConfig::DATA_KEY_DEFAULT_REGISTER:
                    RegisterInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     * @throws KeyInvalidFormatException
     */
    public function checkValueExists($strKey)
    {
        // Check arguments
        KeyInvalidFormatException::setCheck($strKey);

        // Return result
        return $this
            ->getObjRegister()
            ->checkItemExists($strKey);
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getValue($strKey, $default = null)
    {
        // Init var
        $result = (
            $this->checkValueExists($strKey) ?
                $this->getObjRegister()->getItem($strKey) :
                $default
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getTabKey()
    {
        // Return result
        return $this
            ->getObjRegister()
            ->getTabKey();
    }



    /**
     * @inheritdoc
     */
    public function getTabValue()
    {
        // Return result
        return $this
            ->getObjRegister()
            ->getTabItem();
    }



}