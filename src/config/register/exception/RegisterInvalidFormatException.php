<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\config\config\register\exception;

use Exception;

use liberty_code\register\register\api\RegisterInterface;
use liberty_code\config\config\register\library\ConstRegisterConfig;



class RegisterInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $register
     */
	public function __construct($register)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstRegisterConfig::EXCEPT_MSG_REGISTER_INVALID_FORMAT,
            mb_strimwidth(strval($register), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified register has valid format.
	 * 
     * @param mixed $register
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($register)
    {
		// Init var
		$result = ((!is_null($register)) && ($register instanceof RegisterInterface));

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($register);
		}
		
		// Return result
		return $result;
    }
	
	
	
}