<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\config\config\cache\library;



class ConstCacheConfig
{
    // Data constants
    const DATA_KEY_DEFAULT_REPOSITORY = 'objRepository';



    // Exception message constants
    const EXCEPT_MSG_REPOSITORY_INVALID_FORMAT = 'Following repository "%1$s" invalid! It must be a repository object, not null.';
}