<?php
/**
 * Description :
 * This class allows to define cache configuration class.
 * Cache configuration uses specified cache repository, to retrieve value.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\config\config\cache\model;

use liberty_code\config\config\model\DefaultConfig;

use liberty_code\cache\repository\api\RepositoryInterface;
use liberty_code\config\config\exception\KeyInvalidFormatException;
use liberty_code\config\config\cache\library\ConstCacheConfig;
use liberty_code\config\config\cache\exception\RepositoryInvalidFormatException;



/**
 * @method RepositoryInterface getObjRepository() Get repository object.
 * @method void setObjRepository(RepositoryInterface $objRepository) Set repository object.
 */
class CacheConfig extends DefaultConfig
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Constructor / Destructor / Others
	// ******************************************************************************
	
	/**
	 * Constructor
	 *
     * @param RepositoryInterface $objRepository
     */
	public function __construct(RepositoryInterface $objRepository)
	{
		// Call parent constructor
		parent::__construct();

		// Hydrate repository
		$this->setObjRepository($objRepository);
	}
	
	
	
	
	
    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstCacheConfig::DATA_KEY_DEFAULT_REPOSITORY))
        {
            $this->__beanTabData[ConstCacheConfig::DATA_KEY_DEFAULT_REPOSITORY] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstCacheConfig::DATA_KEY_DEFAULT_REPOSITORY
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstCacheConfig::DATA_KEY_DEFAULT_REPOSITORY:
                    RepositoryInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     * @throws KeyInvalidFormatException
     */
    public function checkValueExists($strKey)
    {
        // Check arguments
        KeyInvalidFormatException::setCheck($strKey);

        // Return result
        return $this
            ->getObjRepository()
            ->checkItemExists($strKey);
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     * @throws KeyInvalidFormatException
     */
    public function getValue($strKey, $default = null)
    {
        // Check arguments
        KeyInvalidFormatException::setCheck($strKey);

        // Return result
        return $this
            ->getObjRepository()
            ->getItem($strKey, $default);
    }



    /**
     * @inheritdoc
     */
    public function getTabKey()
    {
        // Return result
        return $this
            ->getObjRepository()
            ->getTabSearchKey();
    }



    /**
     * @inheritdoc
     */
    public function getTabValue()
    {
        // Init var
        $tabKey = $this->getTabKey();
        $result = $this
            ->getObjRepository()
            ->getTabItem($tabKey);

        // Return result
        return $result;
    }



}