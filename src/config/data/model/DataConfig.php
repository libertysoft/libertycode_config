<?php
/**
 * Description :
 * This class allows to define data configuration class.
 * Data configuration uses specified data object, to retrieve value.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\config\config\data\model;

use liberty_code\config\config\model\DefaultConfig;

use liberty_code\data\data\api\DataInterface;
use liberty_code\config\config\exception\KeyInvalidFormatException;
use liberty_code\config\config\data\library\ConstDataConfig;
use liberty_code\config\config\data\exception\DataInvalidFormatException;



/**
 * @method DataInterface getObjData() Get data object.
 * @method void setObjData(DataInterface $objData) Set data object.
 */
class DataConfig extends DefaultConfig
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Constructor / Destructor / Others
	// ******************************************************************************
	
	/**
	 * Constructor
	 *
     * @param DataInterface $objData
     */
	public function __construct(DataInterface $objData)
	{
		// Call parent constructor
		parent::__construct();

		// Hydrate data
		$this->setObjData($objData);
	}
	
	
	
	
	
    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstDataConfig::DATA_KEY_DEFAULT_DATA))
        {
            $this->__beanTabData[ConstDataConfig::DATA_KEY_DEFAULT_DATA] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstDataConfig::DATA_KEY_DEFAULT_DATA
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstDataConfig::DATA_KEY_DEFAULT_DATA:
                    DataInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     * @throws KeyInvalidFormatException
     */
    public function checkValueExists($strKey)
    {
        // Check arguments
        KeyInvalidFormatException::setCheck($strKey);

        // Return result
        return $this
            ->getObjData()
            ->checkValueExists($strKey);
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getValue($strKey, $default = null)
    {
        // Init var
        $result = (
            $this->checkValueExists($strKey) ?
                $this->getObjData()->getValue($strKey) :
                $default
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getTabKey()
    {
        // Return result
        return $this
            ->getObjData()
            ->getTabKey();
    }



    /**
     * @inheritdoc
     */
    public function getTabValue()
    {
        // Return result
        return $this
            ->getObjData()
            ->getTabValue();
    }



}