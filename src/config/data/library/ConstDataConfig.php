<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\config\config\data\library;



class ConstDataConfig
{
    // Data constants
    const DATA_KEY_DEFAULT_DATA = 'objData';



    // Exception message constants
    const EXCEPT_MSG_DATA_INVALID_FORMAT = 'Following data "%1$s" invalid! It must be a data object, not null.';
}