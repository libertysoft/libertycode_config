<?php
/**
 * Description :
 * This class allows to describe behavior of configuration class.
 * Configuration allows to retrieve value, from specified key.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\config\config\api;



interface ConfigInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods check
	// ******************************************************************************
	
	/**
	 * Check if value exists,
     * from specified key.
	 * 
	 * @param string $strKey
	 * @return boolean
	 */
	public function checkValueExists($strKey);



	
	
	// Methods getters
	// ******************************************************************************

	/**
	 * Get value,
     * from specified key.
	 * 
	 * @param string $strKey
     * @param null|mixed $default = null
	 * @return null|mixed
	 */
	public function getValue($strKey, $default = null);



    /**
     * Get index array of keys.
     *
     * @return array
     */
    public function getTabKey();



    /**
     * Get associative array of values.
     * Format: key => value: item.
     *
     * @return array
     */
    public function getTabValue();
}