<?php
/**
 * Description :
 * This class allows to define default configuration class.
 * Can be consider is base of all configuration type.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\config\config\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\config\config\api\ConfigInterface;

use liberty_code\config\config\exception\KeyInvalidFormatException;



abstract class DefaultConfig extends FixBean implements ConfigInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array();
        $result = in_array($key, $tabKey);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidRemove($key, &$error = null)
    {
        // Return result
        return false;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Set check specified key(s) valid.
     *
     * @param string|string[] $key
     * @param boolean $boolArrayEnable = true
     * @throws KeyInvalidFormatException
     */
    protected function setCheckKeyValid($key, $boolArrayEnable = true)
    {
        // Init var
        $boolArrayEnable = (is_bool($boolArrayEnable) ? $boolArrayEnable : true);

        // Case array of keys
        if($boolArrayEnable && is_array($key))
        {
            // Init var
            $tabKey = array_values($key);

            // Run each key
            foreach($tabKey as $key)
            {
                // Validation key
                $this->setCheckKeyValid($key, false);
            }
        }
        // Case one key
        else
        {
            // Validation key
            KeyInvalidFormatException::setCheck($key);
        }
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     * Overwrite it to set specific feature.
     */
    public function getTabValue()
    {
        // Init var
        $tabKey = $this->getTabKey();
        $result = array_combine(
            $tabKey,
            array_map(
                function($strKey) {return $this->getValue($strKey);},
                $tabKey
            )
        );

        // Return result
        return $result;
    }



}