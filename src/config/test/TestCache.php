<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Use
use liberty_code\register\register\memory\model\MemoryRegister;
use liberty_code\cache\format\model\FormatData;
use liberty_code\cache\repository\format\model\FormatRepository;
use liberty_code\cache\repository\format\build\library\ToolBoxFormatBuilder;
use liberty_code\config\config\cache\model\CacheConfig;



// Init var
$tabItem = array(
    'key 1' => 'Value 1',
    'key_2' => 2,
    'key_3' => 3.7,
    'key_4' => true,
    'key_5' => [
        'key 5-1' => 'Value 5-1',
        'key_5-2' => 5.5,
        'key_5-3' => false
    ]
);
$objRegister = new MemoryRegister(null, null);
$objFormatDataGet = new FormatData();
$objFormatDataSet = new FormatData();
$objFormatRepo = new FormatRepository(
    null,
    $objRegister,
    $objFormatDataGet,
    $objFormatDataSet
);
$objConfig = new CacheConfig($objFormatRepo);
ToolBoxFormatBuilder::hydrateSerialFormat($objFormatRepo);
$objFormatRepo->setTabItem($tabItem);



// Test check, get
$tabKey = array(
	'key 1', // Found
	'key_1', // Not found
	'key_2', // Found
    3, // Bad config key format
    null, // Bad config key format
	'key_3', // Found
	'key_4', // Found
    'key_5/key 5-1', // Not found
	'key_5/key_5-2', // Not found
	'key_5', // Found
);

foreach($tabKey as $strKey)
{
	echo('Test check, get "' . strval($strKey)  .'": <br />');
	try{
		echo('<pre>');var_dump($objConfig->checkValueExists($strKey));echo('</pre>');
		echo('<pre>');var_dump($objConfig->getValue($strKey));echo('</pre>');
	} catch(\Exception $e) {
		echo(htmlentities($e->getMessage()));
		echo('<br />');
	}
	echo('<br />');
}

echo('<br /><br /><br />');



// Test get all keys
echo('Test get all keys: <br />');
echo('<pre>');var_dump($objConfig->getTabKey());echo('</pre>');
echo('<br />');

echo('Test get all values: <br />');
echo('<pre>');var_dump($objConfig->getTabValue());echo('</pre>');
echo('<br />');
echo('Test get all values: register:  <br />');
echo('<pre>');print_r($objRegister->getTabItem());echo('</pre>');
echo('<br />');

echo('<br /><br /><br />');


