<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Use
use liberty_code\data\data\table\path\model\PathTableData;
use liberty_code\config\config\data\model\DataConfig;



// Init var
$tabData = array(
    'key 1' => 'Value 1',
    'key_2' => 2,
    'key_3' => 3.7,
    'key_4' => true,
    'key_5' => [
        'key 5-1' => 'Value 5-1',
        'key_5-2' => 5.5,
        'key_5-3' => false
    ]
);
$objData = new PathTableData();
$objConfig = new DataConfig($objData);
$objConfig->getObjData()->setDataSrc($tabData);



// Test check, get
$tabPath = array(
    'key 1', // Found
    'key_1', // Not found
    'key_2', // Found
    3, // Bad config key format
    null, // Bad config key format
    'key_3', // Found
    'key_4', // Found
    'key_5/key 5-1', // Found
    'key_5/key_5-2', // Found
    'key_5', // Found
);

foreach($tabPath as $strPath)
{
	echo('Test check, get "' . strval($strPath) . '": <br />');
	try{
		echo('<pre>');var_dump($objConfig->checkValueExists($strPath));echo('</pre>');
		echo('<pre>');var_dump($objConfig->getValue($strPath));echo('</pre>');
	} catch(\Exception $e) {
		echo(htmlentities($e->getMessage()));
		echo('<br />');
	}
	echo('<br />');
}

echo('<br /><br /><br />');



// Test get all keys
echo('Test get all keys: <br />');
echo('<pre>');var_dump($objConfig->getTabKey());echo('</pre>');
echo('<br />');

echo('Test get all values: <br />');
echo('<pre>');var_dump($objConfig->getTabValue());echo('</pre>');
echo('<br />');

echo('<br /><br /><br />');


